<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

/**
 * Class PopulateSolrCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class PopulateSolrCommand extends Command {

  protected $signature = "tnaflix:populate:solr";

  protected $description = "Populate Solr collection";

  public function handle()
  {

    $client = new \Solarium\Client(config('solarium'));

    // get an update query instance
    $update = $client->createUpdate();

    // add the delete query and delete all documents
    $update->addDeleteQuery('*:*');
    $update->addCommit();

    // executes the query and returns the result
    $client->update($update);

    // Get all videos in chunks of 100 and insert to Solr, this is to avoid too big transactions to Solr

    DB::table('videos')->orderBy('id')->chunk(100, function($videos) use ($client) {

      // get an update query instance
      $update = $client->createUpdate();

      $docs = [];

      foreach($videos as $video) {

        // Create Solr document

        $doc = $update->createDocument();
        $doc->id = $video->id;
        $doc->title = $video->title;
        $doc->description = $video->description;

        // Load video's actors and save to document

        $actor_query = DB::table('video_has_actors')->where('video_id', $video->id)->join('actors', 'actors.id', '=', 'video_has_actors.actor_id')->get();

        foreach($actor_query as $actor) {
          $doc->addField('actors', $actor->title);
        }

        // Load video's tags and save to document

        $tag_query = DB::table('video_has_tags')->where('video_id', $video->id)->join('tags', 'tags.id', '=', 'video_has_tags.tag_id')->get();

        foreach($tag_query as $tag) {
          $doc->addField('tags', $tag->title);
        }

        // Load video's categories and save to document

        $category_query = DB::table('video_has_categories')->where('video_id', $video->id)->join('categories', 'categories.id', '=', 'video_has_categories.category_id')->get();

        foreach($category_query as $category) {
          $doc->addField('categories', $category->title);
        }

        // Set boost values

        $doc->setFieldBoost('actors', 10);
        $doc->setFieldBoost('title', 4);
        $doc->setFieldBoost('tags', 3);
        $doc->setFieldBoost('categories', 2);
        $doc->setFieldBoost('description', 1);

        $docs[] = $doc;

      }

      // Add to query and save

      $update->addDocuments($docs);
      $update->addCommit();

      $client->update($update);

    });

  }
}