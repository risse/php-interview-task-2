<?php

use Illuminate\Support\Facades\DB;

class SolrPopulatingTest extends TestCase
{
  /**
   * Try to populate Solr
   *
   * @return void
   */
  public function testSolrPopulating()
  {

    $client = new \Solarium\Client(config('solarium'));
    // get an update query instance
    $update = $client->createUpdate();

    // this executes the query and returns the result
    $client->update($update);

    // get an update query instance
    $update = $client->createUpdate();

    $docs = [];

    $videos = DB::table('videos')->orderBy('id')->limit(10)->get();

    foreach($videos as $video) {

      $doc = $update->createDocument();
      $doc->id = $video->id;
      $doc->title = $video->title;
      $doc->description = $video->description;

      $actor_query = DB::table('video_has_actors')->where('video_id', $video->id)->join('actors', 'actors.id', '=', 'video_has_actors.actor_id')->get();

      foreach($actor_query as $actor) {
        $doc->addField('actors', $actor->title);
      }

      $tag_query = DB::table('video_has_tags')->where('video_id', $video->id)->join('tags', 'tags.id', '=', 'video_has_tags.tag_id')->get();

      foreach($tag_query as $tag) {
        $doc->addField('tags', $tag->title);
      }

      $category_query = DB::table('video_has_categories')->where('video_id', $video->id)->join('categories', 'categories.id', '=', 'video_has_categories.category_id')->get();

      foreach($category_query as $category) {
        $doc->addField('categories', $category->title);
      }

      // Set boost values

      $doc->setFieldBoost('actors', 10);
      $doc->setFieldBoost('title', 4);
      $doc->setFieldBoost('tags', 3);
      $doc->setFieldBoost('categories', 2);
      $doc->setFieldBoost('description', 1);

      $docs[] = $doc;

    }

    $update->addDocuments($docs);
    $update->addCommit();

    $this->assertInstanceOf(\Solarium\QueryType\Update\Result::class, $client->update($update));

  }
}
