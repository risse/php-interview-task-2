<?php

class SearchPaginationTest extends TestCase
{
  /**
   * Tests that search with pagination works
   *
   * @return void
   */
  public function testSearchPagination()
  {
    $this->get('/search/a?page=5');

    $this->assertJson($this->response->getContent());
  }
}
