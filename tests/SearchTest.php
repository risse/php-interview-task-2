<?php

class SearchTest extends TestCase
{
    /**
     * Tests that search returns valid JSON
     *
     * @return void
     */
    public function testSearch()
    {
        $this->get('/search/something');

        $this->assertJson($this->response->getContent());
    }
}
