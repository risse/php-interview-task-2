## Local development environment

If you want to use the local development environment, you need to have both Docker and [DDEV](https://ddev.readthedocs.io/en/stable/) installed. 

After that, just run:

```
ddev start
```

This installs a local LEMP server with Solr included.

After the installation, the site can be found at <project-name>.ddev.local

For more information, run: `` ddev describe ``

## Configuration

Install composer dependencies: `` composer install `` (if you are using ddev, run `dev ssh` before this)

Copy `.env.example` to `.env`

If you are using ddev, the parameters are:

* mysql hostname: db
* mysql database: db
* mysql username: db
* mysql password: db
* solr hostname: solr
* solr port: 8983
* solr path: "/"
* solr core: dev

Solr config files can be found at `.ddev/solr/`

## Running 

To populate the Solr database, SSH into the environment with `ddev ssh` and run:
```
php artisan tnaflix:populate:solr
```

The source code for this command can be found at `app/Console/Commands/PopulateSolrCommand.php`

The search is available at `<project-name>.ddev.local/search/<search-term-here>` Pagination can be used by appending `?page=2` to the end of the url

Source code for search is at `routes/web.php`

PHPUnit can be run by SSH'ing and running `./vendor/bin/phpunit`, tests are in `tests` folder.
