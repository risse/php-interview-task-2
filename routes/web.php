<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Http\Request;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// Search endpoint

$router->get('/search/{search}', function($search, Request $request) {

  // Set the pager number

  if($request->input('page')) $page = $request->input('page'); else $page = 1;

  // How many results per page

  $per_page = 20;

  $client = new \Solarium\Client(config('solarium'));

  // get a select query instance
  $query = $client->createSelect();

  // Search is urlencoded, decode it

  $search = urldecode($search);

  // Query all the search parameter in all fields we are interested in

  $query->setQuery("actors:%P1% OR title:%P1% OR categories:%P1% OR tags:%P1% OR description:%P1%", [$search]);

  // Set query start and amount

  $query->setStart(($page  -1) * $per_page)->setRows($per_page);

  // Get results

  $resultset = $client->select($query);

  $documents = [];

  // Loop through documents and add them to array, which is returned to user

  foreach($resultset->getDocuments() as $document) {
    $documents[] = $document->getFields();
  }

  return response()->json($documents);

});
